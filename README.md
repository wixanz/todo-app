# Todo Application #

This project illustrates the basic usage of Spring REST + AngularJS + MongoDB technologies follow best practices. 

### Development enviroment: ###
- Intellij IDEA
- JDK 1.8

### How to download the project? ###

* You can download this project following to Downloads -> Branches -> On the right side of 'master' row you will see .zip, .bz archive formats to download it.
* Moreover, you can just clone it via Git.